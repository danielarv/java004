package deloitte.academy.lesson01.entity;

public enum Validacion {

	EMPLEADO("El ID de Empleado si existe en el sistema.");
	
	//Atributo
	private String descripcion;
	
	private Validacion () {
		
	}
	
	//Constructor
	private Validacion(String descripcion) {
		this.descripcion = descripcion;
	}

	//Getters y Setters
	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}


package deloitte.academy.lesson01.entity;

/**
 * Esta clase declara los atributos, constructores, getters and setters
 * 
 * @author dareyes
 *
 */
public abstract class CheckIn {

	// Declaracion de atributos
	private String fechaDeIngreso;
	private String huesped;
	private int habitacion;
	private int dias;
	private double importePorDia;

	// Constructores
	public CheckIn() {

	}

	public CheckIn(String fechaDeIngreso, String huesped, int habitacion, int dias, double importePorDia) {
		super();
		this.fechaDeIngreso = fechaDeIngreso;
		this.huesped = huesped;
		this.habitacion = habitacion;
		this.dias = dias;
		this.importePorDia = importePorDia;
	}

	// Getters and Setters de los Atributos
	public String getFechaDeIngreso() {
		return fechaDeIngreso;
	}

	public void setFechaDeIngreso(String fechaDeIngreso) {
		this.fechaDeIngreso = fechaDeIngreso;
	}

	public String getHuesped() {
		return huesped;
	}

	public void setHuesped(String huesped) {
		this.huesped = huesped;
	}

	public int getHabitacion() {
		return habitacion;
	}

	public void setHabitacion(int habitacion) {
		this.habitacion = habitacion;
	}
	

	public int getDias() {
		return dias;
	}

	public void setDias(int dias) {
		this.dias = dias;
	}
	
		public double getImportePorDia() {
		return importePorDia;
	}

	public void setImportePorDia(double importePorDia) {
		this.importePorDia = importePorDia;
	}

		//Crear metodo abstracto para que otra clase lo utilice
		public abstract double cobrar();
		

		


		}


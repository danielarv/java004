package deloitte.academy.lesson01.logic;

import java.util.logging.Logger;
import deloitte.academy.lesson01.entity.CheckIn;
import deloitte.academy.lesson01.entity.Validacion;

/**
 * Esta clase hereda los atributos de CheckIn y genera un descuento en el metodo cobrar
 * @author dareyes
 *
 */

public class Empleado extends CheckIn {
	final Logger LOGGER = Logger.getLogger(Empleado.class.getCanonicalName());
	
	private int id;
	
	

	public Empleado(int id) {
		super();
		this.id = id;
	}

	public Empleado() {
		super();
		// TODO Auto-generated constructor stub
	}


	public Empleado(String fechaDeIngreso, String huesped, int habitacion, int dias, double importePorDia) {
		super(fechaDeIngreso, huesped, habitacion, dias, importePorDia);
		// TODO Auto-generated constructor stub
	}



	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Override
	public double cobrar() {
		
		double total = this.getImportePorDia() * .70;
		
		return total*this.getDias();
	}

		//Metodo de validacion para numero de ID de empleado
		public int validacionEmpleado() {
		
		if ((id >= 4000) && (id <= 4999)) {
			System.out.println(Validacion.EMPLEADO.getDescripcion());
		} else
			LOGGER.info("El ID de empleado no existe en el sistema.");
		return id;
	}

		
		}


	
	
	
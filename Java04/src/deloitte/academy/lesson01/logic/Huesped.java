package deloitte.academy.lesson01.logic;

import deloitte.academy.lesson01.entity.CheckIn;

/**
 * Esta clase hereda los atributos de CheckIn y crea el metodo de cobro para huesped 
 * Se crea el descuento si la estadia en el hotel es mayor a 5 dias
 * @author dareyes
 *
 */
public class Huesped extends CheckIn {
	
	public Huesped() {
		
	}

	
	public Huesped(String fechaDeIngreso, String huesped, int habitacion, int dias, double importePorDia) {
		super(fechaDeIngreso, huesped, habitacion, dias, importePorDia);
		// TODO Auto-generated constructor stub
	}


	/* 
	 * Este metodo aplica un metodo de 10 % de descuento si se queda 5 dias o mas
	 * Si no, el precio normal por dia
	 */
	@Override
	public double cobrar() {
		double total;
		
		if (this.getDias() >= 5) {
		  total = this.getImportePorDia() * .90;
		  System.out.println("Descuento aplicado del 10%");
		} else {
		 total = this.getImportePorDia();
		}
		
		return total*this.getDias();

}
}

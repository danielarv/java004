package deloitte.academy.lesson01.run;

import java.util.ArrayList;


import deloitte.academy.lesson01.entity.CheckIn;
import deloitte.academy.lesson01.logic.Empleado;
import deloitte.academy.lesson01.logic.Huesped;


/**
 * Esta clase ejecuta los metodos para checar disponibilidad de habitaciones y
 * registrar huespedes
 * 
 * @author dareyes
 *
 */

public class Ejecutar {

	public static void main(String[] args) {

		// Crear registro de personas que haran check in
		Huesped registroHuesped = new Huesped("06/03/2020", "Daniela Reyes", 101, 3, 1200);
		Huesped registroHuesped2 = new Huesped("04/03/2020", "Alfredo Reyes", 202, 6, 1800);

		// Crear registro de empleados que haran check in
		Empleado registroEmpleado = new Empleado("01/03/2020", "Jesus Pe�a", 303, 2, 1200);
		registroEmpleado.setId(4001);
		Empleado registroEmpleado2 = new Empleado("08/03/2020", "Gustavo Luna", 404, 8, 1500);
		registroEmpleado2.setId(8000);

		// Imprime los datos del primer registro de huesped
		System.out.println("--------------------REGISTRO DE HUESPEDES--------------------" + "\n");

		System.out.println("El huesped " + registroHuesped.getHuesped() + " se registr� el d�a "
				+ registroHuesped.getFechaDeIngreso() + " en la habitaci�n " + registroHuesped.getHabitacion() + ".");
		System.out.println("Su estancia ser� por " + registroHuesped.getDias());

		ejecutarPago(registroHuesped);

		System.out.println("\n");
		// Imprime los datos del segundo registro de huesped
		System.out.println("El huesped " + registroHuesped2.getHuesped() + " se registr� el d�a "
				+ registroHuesped2.getFechaDeIngreso() + " en la habitaci�n " + registroHuesped2.getHabitacion() + ".");
		System.out.println("Su estancia ser� por " + registroHuesped2.getDias());

		ejecutarPago(registroHuesped2);

		System.out.println("\n" + "--------------------REGISTRO DE EMPLEADOS--------------------" + "\n");
		System.out.println("\n");
		// Imprime los datos del primer registro de empleado
		System.out.println("El empleado " + registroEmpleado.getHuesped() + " se registr� el d�a "
				+ registroEmpleado.getFechaDeIngreso() + " en la habitaci�n " + registroEmpleado.getHabitacion() + ".");
		System.out.println("Su estancia ser� por " + registroEmpleado.getDias());
		System.out.println("Numero de empleado: " + registroEmpleado.getId());

		ejecutarPago(registroEmpleado);

		// Validacion de id de empleado
		validar(registroEmpleado);

		System.out.println("\n");
		// Imprime los datos del primer registro de empleado
		System.out.println("El empleado " + registroEmpleado2.getHuesped() + " se registr� el d�a "
				+ registroEmpleado2.getFechaDeIngreso() + " en la habitaci�n " + registroEmpleado2.getHabitacion()
				+ ".");
		System.out.println("Su estancia ser� por " + registroEmpleado2.getDias());
		System.out.println("Numero de empleado: " + registroEmpleado2.getId());

		ejecutarPago(registroEmpleado2);

		// Validacion de id de empleado
		validar(registroEmpleado2);

		// Disponibilidad de habitaciones
		ArrayList<Integer> HabitacionesDisponibles = new ArrayList<Integer>();
		HabitacionesDisponibles.add(102);
		HabitacionesDisponibles.add(205);
		HabitacionesDisponibles.add(407);

		System.out.println("\n");
		System.out.println("Habitaciones Disponibles");
		HabitacionesDisponibles.forEach(n -> System.out.println(n));

		// Check out de habitaciones
		String mensaje = "";
		for (int posicion : HabitacionesDisponibles) {
			if (registroHuesped.getHabitacion() == posicion) {

				mensaje = "La habitacion esta disponible. Registra al huesped.";
				break;
			} else {
				mensaje = "Por el momento la habitacion no se encuentra disponible.";
			}
		}
		System.out.println(mensaje);

	}

	// METODOS

	// Metodo para hacer el cobro de empleados y huespedes
	public static void ejecutarPago(CheckIn objeto) {
		System.out.println("Total a pagar: " + objeto.cobrar() + " pesos.");
	}

	// Metodo para validar numero de empleado
	public static void validar(Empleado objeto2) {
		System.out.println(objeto2.validacionEmpleado());
	}

}
